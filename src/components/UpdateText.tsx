import * as React from 'react';
import {Button, Text, TextInput, View} from 'react-native';
import {
  initialState,
  UpdateTextReducer,
} from '../redux/reducers/UpdateTextReducer';
import {UpdateTextActions} from '../redux/actions/UpdateText';

export const UpdateText = props => {
  const [state, dispatch] = React.useReducer(UpdateTextReducer, initialState);
  const inputRef = React.useRef(null);
  return (
    <View>
      <Text style={{textAlign: 'center'}}>{state.textValue}</Text>
      <TextInput
        ref={inputRef}
        style={{height: 40, width: 300, borderWidth: 1, margin: 10}}
        placeholder={'Enter your Text'}
        onChangeText={val => dispatch(UpdateTextActions.updateTextInput(val))}
        value={state.textInputValue}
      />
      <Button
        title={'update Text'}
        onPress={() => {
          dispatch(UpdateTextActions.updateText(state.textInputValue));
          // console.log(inputRef.current.focus());
        }}
      />
    </View>
  );
};
