export enum UpdateTextActionType {
  TEXT_INPUT_UPDATE = 'Text input update',
  TEXT_UPDATE = 'Text update',
}

export class UpdateTextActions {
  static updateTextInput(value) {
    return {
      type: UpdateTextActionType.TEXT_INPUT_UPDATE,
      value: value,
    };
  }
  static updateText(value) {
    return {
      type: UpdateTextActionType.TEXT_UPDATE,
      value: value,
    };
  }
}
