import {combineReducers} from 'redux';
import {UpdateTextReducer} from './UpdateTextReducer';

const rootReducer = combineReducers({
  updateTextReducer: UpdateTextReducer,
});

export default rootReducer;
