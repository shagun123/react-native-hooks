import {UpdateTextActionType} from '../actions/UpdateText';

interface State {
  textValue: string;
  textInputValue: string;
}

export const initialState: State = {
  textValue: 'This is my Home screen',
  textInputValue: '',
};

export const UpdateTextReducer = (state = initialState, action): State => {
  switch (action.type) {
    case UpdateTextActionType.TEXT_UPDATE: {
      return {...state, textValue: action.value};
    }
    case UpdateTextActionType.TEXT_INPUT_UPDATE: {
      return {...state, textInputValue: action.value};
    }
    default: {
      return state;
    }
  }
};
