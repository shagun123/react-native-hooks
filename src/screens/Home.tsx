import * as React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {UpdateText} from '../components/UpdateText';

class Home extends React.Component<any, any> {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <UpdateText />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Home;
