import * as React from 'react';
import Home from './src/screens/Home';
import {Provider} from 'react-redux';
import Store from './src/redux/Store';
class App extends React.Component<any, any> {
  componentDidMount(): void {}

  render() {
    return (
      <Provider store={Store}>
        <Home />
      </Provider>
    );
  }
}

export default App;
